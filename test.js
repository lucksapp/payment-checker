// https://api.coinmarketcap.com/v2/ticker/1567/

// procura no dynamodb quais sao os pending payments
// organiza os dados, de modo que things com 2 pending payments sejam somados
// procura no shadow de cada thing, se um endereço de wallet foi adicionado
// caso nao tenha sido, deleta da variavel
// caso sim,
// insere em wallet address na variavel
// acessa API do coinmarketcap pra saber qual valor do NANO
// pega cada thing, converte o valor USD em NANO
// envia o pagamento
// armazena em variavel controle que se o pagamento foi bem sucedido
// nos payments bem sucedidos, 
// atualiza dynamodb (claimed) e shadow (notificação e newWallet)
// nos payments falhos, atualiza shadow pedindo novo endereço

var AWS = require('aws-sdk');
const iot = new AWS.Iot();
const axios = require("axios");
const {Nano} = require('nanode')
const nano = new Nano({apiKey: '2912c871-5c86-11e8-a943-ad525fb38e32'})

void async function(){
	
	AWS.config.update({region: 'us-east-1'});
	var ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
	
	// retorna todos os registros claimed = 0
	let params = {
	ExpressionAttributeValues: {
		":c": {
		N: "0"
		},
	},
	ExpressionAttributeNames: {		 
		"#v": "value",
		"#d": "date",
		"#r": "refer",
		"#t": "thingName"
	},
	FilterExpression: "claimed = :c",
	ProjectionExpression: "#v, #d, #r, #t",
	TableName: "payments"
	};

	let data;

	try{
		data = await ddb.scan(params).promise();
	}
	catch (e){
		console.log(e)
	}

	// variavel contendo apenas uma entrada pra cada thing
	// (pode acontecer de sortear 2x a mesma pessoa)
	let things = {};

	data.Items.forEach(function(v, k){
		if (!things[v.thingName.S]){
			things[v.thingName.S] = {};
			things[v.thingName.S].thingName = v.thingName.S;
			things[v.thingName.S].value = Number(v.value.N);
			things[v.thingName.S].key_pair = [{date: v.date.S, refer: v.refer.N}];
		}
		else {
			things[v.thingName.S].value = Number(v.value.N) + things[v.thingName.S].value;
			things[v.thingName.S].key_pair.push({date: v.date.S, refer: v.refer.N});
		}
	});
	
	if (things.length == 0){
		return;
	}
	
	// chama a interface com os dados iot
  	let iotdata = new AWS.IotData({endpoint: 'a8qu084ib8lgi.iot.us-east-1.amazonaws.com'});	
   
    // shadows dos thing
	let shadows;
	// iterar nos things com pending payment	
	for (let thingName in things){
		
		let wallet = false;
		try {
			let shadow = await iotdata.getThingShadow({thingName: thingName}).promise();
			shadow = JSON.parse(shadow.payload);
			if (shadow.state.desired.wallet != ''){
				things[thingName].wallet = shadow.state.desired.wallet;
				wallet = true;
				things[thingName].shadow = shadow;
				delete things[thingName].shadow.state.delta;
			}
		}
		catch (e){
			  console.log("Erro ao getThingShadow do ganhador ", e);
			  delete things[thingName];
		}
		// exclui todos things que nao inseriram wallet
		if (!wallet){
			delete things[thingName];
		}
	}

	// se nenhum deles inseriu a wallet, para a função
	if (things.length == 0){
		return;
	}
	
	// acessa API coinmarketcap pra saber o preço USD da NANO
	const url = "https://api.coinmarketcap.com/v2/ticker/1567/";
	let nano_price;
	try {
		const response = await axios.get(url);
		nano_price = response.data.data.quotes.USD.price;
	} catch (error) {
		console.log(error);
	}

	const pk = '982E5AA4E284DDC588962D16A2036D8AA752AD46B771CF9F3F396ABC3A029BD2';

	try {
		await nano.account(pk).receive()
	}
	catch (e) {
		// n é necessario tratar erros aqui
		// esse código apenas automatiza recebimento de fundos
		// pra nao ter que fazer manualmente
	}
	for (let thingName in things){
		const wallet = things[thingName].wallet;
		const amount = things[thingName].value * nano_price;

		try {
			await nano.account(pk).send(amount, wallet)
		}
		catch (e) {
			console.log(e);

			// retornar bad wallet
			if (e.response.data.error === "Bad destination account"){
				// alterar shadow
				things[thingName].shadow.state.desired.wallet = '';
				things[thingName].shadow.state.desired.badWallet = true;

				const shadow = things[thingName].shadow;
				try {
					var updateShadow = await iotdata.updateThingShadow({thingName: thingName, payload: Buffer.from(JSON.stringify(shadow))}).promise();
				}
				catch (e){
					console.log("Erro ao updateThingShadow de badwallet ", e);
				}
			}

			// só fica em things os pagamentos concluidos
			delete things[thingName];
		}
	}

	// se nenhum pagamento deu certo, fecha programa
	if (things.length == 0){
		return;
	}
	
	// rodar things para enviar notificação de pagamento no shadow
	// e alterar o status no dynamodb

	for (let thingName in things){

		for (let key in things[thingName].key_pair){
			let date = things[thingName].key_pair[key].date;
			let refer = things[thingName].key_pair[key].refer;

			params = {
				TableName: 'payments',
				UpdateExpression: "SET claimed = :c",
				ExpressionAttributeValues:{
					":c": {N: '1'}
				},
				Key:{
					"date": {"S": date},
					"refer": {"N": refer}
				},
				ReturnValues:"UPDATED_NEW"
			};
			
			//let data = 1;
			let data;
			try{
				data = await ddb.updateItem(params).promise();
			}
			catch (e){
				console.log("nao atualizou dynamodb ", e)
				console.log("crítico. pagamento foi feito mas contiua unclaimed");
			}
			if (!data){
				delete things[thingName];
			}
		}

	}

	for (let thingName in things){
		things[thingName].shadow.state.desired.wallet = '';
		things[thingName].shadow.state.desired.paymentMade = true;

		const shadow = things[thingName].shadow;
		try {
			var updateShadow = await iotdata.updateThingShadow({thingName: thingName, payload: Buffer.from(JSON.stringify(shadow))}).promise();
		}
		catch (e){
			console.log("Erro ao updateThingShadow de pagamento ", e);
		}
	}


}();