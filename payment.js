// https://api.coinmarketcap.com/v2/ticker/1567/

var s = require('./send-funds');
var AWS = require('aws-sdk');

let getContent = function(url) {
	  // return new pending promise
	  return new Promise((resolve, reject) => {
		// select http or https module, depending on reqested url
		const lib = url.startsWith('https') ? require('https') : require('http');
		const request = lib.get(url, (response) => {
		  // handle http errors
		  if (response.statusCode < 200 || response.statusCode > 299) {
			 reject(new Error('Failed to load page, status code: ' + response.statusCode));
		   }
		  // temporary data holder
		  const body = [];
		  // on every content chunk, push it to the data array
		  response.on('data', (chunk) => body.push(chunk));
		  // we are done, resolve promise with those joined chunks
		  response.on('end', () => resolve(body.join('')));
		});
		// handle connection errors of the request
		request.on('error', (err) => reject(err))
		})
};  
    
callDatabase = async function (query){
	const rds_param = {
		FunctionName: 'db-handler-dev-db_handler',
		Payload: JSON.stringify(query)
	}

	let res;
	try {
		res = await lambda.invoke(rds_param).promise();
		return JSON.parse(res.Payload);
	} catch (e){
		console.error("Erro fatal! Call database: ", query, e);
		return e;
	}
}

getPaymentsUnclaimed = async function(){
	const query = {
		type: "select",
		table: "payments",
		columns: ["name", "value"],
		where: [{"arbitrary": "claim_date is null"}]
	}
	let rows = (await callDatabase(query)).rows;

	rows = (rows.length==0) ? false : rows;

	return rows;
}

sumValuesFromSameUser = function(unclaimeds){
	let u = [];

	unclaimeds.forEach(function(v){
		if (!u[v.name]){
			u[v.name] = {value: 0}
		}

		u[v.name].value = u[v.name].value + v.value;
	});
	
	return u;
}

fillWalletAddress = async function(unclaimeds){

	// chama a interface com os dados iot
	AWS.config.update({region: 'us-east-1'});
	let iotdata = new AWS.IotData({endpoint: 'a8qu084ib8lgi.iot.us-east-1.amazonaws.com'});	

	// iterar nos things com pending payment
	//Object.keys(unclaimeds).forEach(async function(name){	
	for (let name in unclaimeds){
		
		let value = unclaimeds[name];
		try {
			let shadow = await iotdata.getThingShadow({thingName: name}).promise();
			shadow = JSON.parse(shadow.payload);
			// armazena wallet e shadow na var
			/**
			 * unclaimeds = {
			 * 		"name": {
			 * 				value: 1.23,
			 * 				wallet: 'xrb_...',
			 * 				shadow: {...}
			 * 		}
			 * }
			 */
			if (shadow.state.desired.wallet != ''){
				unclaimeds[name].wallet = shadow.state.desired.wallet;
				unclaimeds[name].shadow = shadow;
				delete unclaimeds[name].shadow.state.delta;
			} else {
				delete unclaimeds[name];
			}
		}
		catch (e){
			  console.log("Erro ao getThingShadow do ganhador ", e);
			  delete unclaimeds[name];
		}
	};

	// se nenhum deles inseriu a wallet, para a função
	if (Object.keys(unclaimeds).length == 0){
		return false;
	}
	return unclaimeds;
}

convertValueToNANO = async function(unclaimeds){
	const url = "https://api.coinmarketcap.com/v2/ticker/1567/";
	let nano_price;
	let request_failed = false;

	do {
		try {
			let response = await getContent(url);
			response = JSON.parse(response);

			nano_price = response.data.quotes.USD.price;
			request_failed = true;
		} catch (e) {
			// try again
		}
	} while(!request_failed);

	for (let name in unclaimeds){
		unclaimeds[name].value = unclaimeds[name].value / nano_price;
	};

	return unclaimeds;
}

sendPayments = async function(unclaimeds){

	let payments = {failed: [], success: []};

	for (let name in unclaimeds){
		const wallet = unclaimeds[name].wallet;
		const amount = unclaimeds[name].value;

		const rds_param = {
			FunctionName: 'send-funds-dev-send',
			Payload: JSON.stringify({amount: amount, wallet: wallet})
		}
	
		let res;
		try {
//			res = await lambda.invoke(rds_param).promise();
			res = await s.sendFunds(amount, wallet);
			console.log(res);
			if (res.success){
				payments.success[name] = unclaimeds[name];
			} else {
				if (res.error === "insufficient"){
					console.error("Erro fatal! Insufficient balance");
				} else if (res.error === "invalid") {
					payments.failed[name] = unclaimeds[name];
				} else {
					console.error("Erro fatal! send funds unknown: ", res.log);
				}
			}
		} catch (e){
			console.error("Erro fatal! Call send funds: ", e);
		}
	}

	return payments;
}

sendBadWalletNotify = async function(payments){
	AWS.config.update({region: 'us-east-1'});
	let iotdata = new AWS.IotData({endpoint: 'a8qu084ib8lgi.iot.us-east-1.amazonaws.com'});	


	for (let name in payments){

		// alterar shadow
		payments[name].shadow.state.desired.wallet = '';
		payments[name].shadow.state.desired.badWallet = true;

		const shadow = payments[name].shadow;
		try {
			await iotdata.updateThingShadow({thingName: name, payload: Buffer.from(JSON.stringify(shadow))}).promise();
		}
		catch (e){
			console.log("Erro fatal! Badwallet: ", e);
		}	
	}
}

sendPaymentNotify = async function(payments){
	AWS.config.update({region: 'us-east-1'});
	let iotdata = new AWS.IotData({endpoint: 'a8qu084ib8lgi.iot.us-east-1.amazonaws.com'});	


	for (let name in payments){

		// alterar shadow
		payments[name].shadow.state.desired.wallet = '';
		payments[name].shadow.state.desired.paymentMade = true;

		const shadow = payments[name].shadow;
		try {
			await iotdata.updateThingShadow({thingName: name, payload: Buffer.from(JSON.stringify(shadow))}).promise();
		}
		catch (e){
			console.log("Erro fatal! PaymentMade: ", e);
		}	
	}
}

setPaymentsClaimed = async function(payments){
	const date = new Date().toJSON().slice(0,10);

	for (let name in payments){
		const query = {
			type: "update",
			table: "payments",
			set: {"claim_date": date},
			where: [{"arbitrary": "claim_date is null"}, {"column": "name", "value": name, "logical": "AND"}]
		}
		await callDatabase(query);
	}
}

void async function(){

	// connect to RDS and get unclaimed payments order
	let unclaimeds = await getPaymentsUnclaimed();

	// if no payment order stop
	if (!unclaimeds){
		return;
	}

	// if user won two or more times
	// sum up its values
	unclaimeds = sumValuesFromSameUser(unclaimeds);

	// connects to user's shadow and get wallet address
	unclaimeds = await fillWalletAddress(unclaimeds);

	// if no user has put their w. address stop
	if (!unclaimeds){
		return;
	}
	
	// access coinmarketcap api and convert unclaimed value to NANO
	unclaimeds = await convertValueToNANO(unclaimeds);
	
	// send value to wallet
	let payments = await sendPayments(unclaimeds);
	if (Object.keys(payments.failed).length == 0){
		if (Object.keys(payments.success).length == 0){
			return;
		}
	}

	// notify user that wallet address is malformed/not found
	sendBadWalletNotify(payments.failed);

	
	if (Object.keys(payments.success).length == 0){
		return;
	}

	// notify user that payment has been made
	sendPaymentNotify(payments.success);

	// update RDS set claimed = true
	setPaymentsClaimed(payments.success);

}();
